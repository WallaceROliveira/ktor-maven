-Rodar comando para criar network para o banco de dados: `docker network create --driver bridge postgres-network`

- Rodar docker do banco de dados com o comando:
`docker run --name postgreskotlinmaven --network postgres-network -e POSTGRES_USER=wall -e POSTGRES_PASSWORD=wall -e POSTGRES_DB=kotlin_maven -p 5431:5432 -d postgres`

- Rodar o comando: `mvn clean install`
 
- Rodar a aplicação: `java -jar target/ktor-0.0.1-jar-with-dependencies.jar`
