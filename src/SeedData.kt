package br.com.wall


import br.com.wall.car.Cars
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import java.util.*

object SeedData {

    fun load() {
        transaction {
            if (Cars.selectAll().count() == 0) {
                Cars.deleteWhere { Cars.brand like "%%" }

                Cars
                for (i in 1..1000)
                    Cars.insert {
                        it[id] = UUID.randomUUID()
                        it[brand] = "Tesla-${i}"
                        it[model] = "Model #${i}"
                    }
            }
        }
    }
}
