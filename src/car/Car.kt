package br.com.wall.car

import java.util.*

data class Car(
    val id: UUID? = null,
    var brand: String,
    var model: String
)
