package br.com.wall.car

import br.com.wall.ServiceHelper
import org.jetbrains.exposed.sql.*
import org.joda.time.DateTime
import java.util.*

object Cars : Table() {
    val id = uuid("id").primaryKey()
    val brand = text("brand")
    val model = text("model")
}

class CarService {

    suspend fun getAllCars(): List<Car> = ServiceHelper.dbQuery {
        Cars.selectAll().map { toCar(it) }
    }

    suspend fun getCar(id: UUID): Car? = ServiceHelper.dbQuery {
        Cars.select {
            (Cars.id eq id)
        }.mapNotNull { toCar(it) }
            .singleOrNull()
    }

    suspend fun addCar(car: Car): Car {
        val randomUUID = UUID.randomUUID()
        ServiceHelper.dbQuery {
            Cars.insert {
                it[id] = randomUUID
                it[brand] = car.brand
                it[model] = car.model
            }
        }
        return getCar(randomUUID)!!
    }

    suspend fun updateCar(car: Car): Car {
        val id = car.id
        return if (id == null) {
            addCar(car)
        } else {
            ServiceHelper.dbQuery {
                Cars.update({ Cars.id eq id }) {
                    it[Cars.id] = car.id
                    it[brand] = car.brand
                    it[model] = car.model
                }
            }
            getCar(id)!!
        }
    }

    suspend fun deleteCar(id: UUID): Boolean = ServiceHelper.dbQuery {
        Cars.deleteWhere { Cars.id eq id } > 0
    }

    private fun toCar(row: ResultRow): Car =
        Car(
            id = row[Cars.id],
            brand = row[Cars.brand],
            model = row[Cars.model]
        )
}
