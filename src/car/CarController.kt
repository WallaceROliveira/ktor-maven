package br.com.wall.car

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import java.util.*

fun Route.car(carService: CarService) {

    route("/car") {

        get("/") {
            call.respond(carService.getAllCars())
        }

        get("/{id}") {
            val car = carService.getCar(UUID.fromString(call.parameters["id"]))
            if (car == null) call.respond(HttpStatusCode.NotFound)
            else call.respond(car)
        }

        post("/") {
            val car = call.receive<Car>()
            call.respond(carService.addCar(car))
        }

        put("/") {
            val car = call.receive<Car>()
            call.respond(carService.updateCar(car))
        }

        delete("/{id}") {
            val removed = carService.deleteCar(UUID.fromString(call.parameters["id"]))
            if (removed) call.respond(HttpStatusCode.OK)
            else call.respond(HttpStatusCode.NotFound)
        }
    }
}
