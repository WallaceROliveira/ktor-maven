package br.com.wall.coroutinescope

import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.route

fun Route.coroutineScope(coroutineScopeService: CoroutineScopeService) {

    route("/coroutineScope") {

        get("/") {
            call.respond(coroutineScopeService.getCoroutine())
        }
    }
}
