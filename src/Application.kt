package br.com.wall

import br.com.wall.car.CarService
import br.com.wall.car.car
import br.com.wall.coroutinescope.CoroutineScopeService
import br.com.wall.coroutinescope.coroutineScope
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.*
import io.ktor.http.ContentType
import io.ktor.jackson.jackson
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database


fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    initDB()
    install(Compression)
    install(DefaultHeaders)
    install(CallLogging)

    install(ContentNegotiation) {
        jackson {
            registerModule(JodaModule())
            enable(SerializationFeature.INDENT_OUTPUT)
            disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            setSerializationInclusion(JsonInclude.Include.NON_NULL)
            configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        }
    }

    install(CORS) {
        anyHost()
    }

    install(Routing) {

        car(CarService())

        coroutineScope(CoroutineScopeService())

        get("/") {
            call.respondText("Response de Teste!", contentType = ContentType.Text.Plain)
        }
    }
}

fun initDB() {
    val config = HikariConfig("/hikari.properties")
    val ds = HikariDataSource(config)
    var data = Database.connect(ds)
    Flyway.configure().dataSource("jdbc:postgresql://localhost:5431/kotlin_maven","wall", "wall").load().migrate()

    //Descomentar para rodar os inserts de teste
    //SeedData.load()
}
