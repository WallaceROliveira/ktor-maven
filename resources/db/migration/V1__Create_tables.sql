create table if not exists cars(
    id uuid not null,
    brand varchar not null,
    model varchar not null
);
